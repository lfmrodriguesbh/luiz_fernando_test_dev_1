import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private http: HttpClient, private router: Router) {
  }

  getList(): Observable<any> {
    let headers = new HttpHeaders().append("Content-Type", "application/json")
    .append("Host", "localhost:4200");
    return this.http.get<any>("http://localhost:8080/company/");
  }
}

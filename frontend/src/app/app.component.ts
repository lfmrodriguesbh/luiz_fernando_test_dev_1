import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyService } from './services/company.service';
import { Observable } from 'rxjs';
import { Company } from './models/Company';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
 companies: Observable<Company[]>;

  constructor(
    private routeActive: ActivatedRoute,
    private companyService: CompanyService,
    private router: Router
  ) { }
  
  ngOnInit(): void {
    this.companyService.getList().subscribe(data => {
      this.companies = data;
    });
  }
  candidate = 'Luiz Fernando Moreira Rodrigues';
}

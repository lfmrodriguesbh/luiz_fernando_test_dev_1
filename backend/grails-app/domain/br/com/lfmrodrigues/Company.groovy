package br.com.lfmrodrigues

class Company {

    static transients = ['stdPrice']
    static hasMany = [stocks:Stock]

    String name
    String segment

    static constraints = {
        name nullable: false, blank: false
        segment nullable: false, blank: false    
    }


    static mapping = { stocks lazy: false }
}

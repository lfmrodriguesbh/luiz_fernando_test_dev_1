package br.com.lfmrodrigues

class Stock {
    Double price
    Date price_date
    Company company
    
    static constraints = {
        price nullable: false, blank: false
        price_date nullable: false, blank: false
        company nullable: false, blank: false
    }
}
package backend
import br.com.lfmrodrigues.CompanyService
import br.com.lfmrodrigues.StockService
import br.com.lfmrodrigues.Company
import br.com.lfmrodrigues.Stock

class BootStrap {

    CompanyService companyService
    StockService stockService

    def init = { servletContext ->


        Company company1 = new Company()
        company1.name = 'Fiat'
        company1.segment = 'Vehicle'
        companyService.save(company1)

        Company company2 = new Company()
        company2.name = 'Chevrolet'
        company2.segment = 'Vehicle'
        companyService.save(company2)

        Company company3 = new Company()
        company3.name = 'Ford'
        company3.segment = 'Vehicle'
        companyService.save(company3)


        use(groovy.time.TimeCategory) {
            0.upto(31 * 24 * 60){
                def lastMonth = new Date() - it.minutes
                println lastMonth

                Stock stock1 = new Stock()
                stock1.price = it * 500 // Generate a random number to populate a price
                stock1.price_date = lastMonth
                stock1.company = company1


                Stock stock2 = new Stock()
                stock2.price = it * 700 // Generate a random number to populate a price
                stock2.price_date = lastMonth
                stock2.company = company2

                Stock stock3 = new Stock()
                stock3.price = it * 1000 // Generate a random number to populate a price
                stock3.price_date = lastMonth
                stock3.company = company3

                stockService.save(stock1)
                stockService.save(stock2)
                stockService.save(stock3)
            }
        }

        
    }
    def destroy = {
    }
}
